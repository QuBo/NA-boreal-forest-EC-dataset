# Summary

A boreal forest model benchmarking dataset for North America: a case study with the Canadian Land Surface Scheme including Biogeochemical Cycles (CLASSIC)

# Boreal forest stands

Ameriflux-ID: name / location / climate / permafrost  
CA-Qfo: Quebec-Eastern Boreal, Mature Black Spruce / Quebec, Canada (74.34°W, 49.69°N) / -0.4℃, 962mm / Permafrost free  
CA-Obs: Saskatchewan-Western Boreal, Mature Black Spruce / Saskatchewan, Canada (105.12°W, 53.99°N) / 0.8℃, 406mm / Permafrost free  
CA-Man: Manitoba-Northern Old Black Spruce / Manitoba, Canada (98.48°W, 55.88°N) / -3.2℃, 520mm	/ Permafrost free  
CA-SMC: Smith Creek / Northwest Territories, Canada (123.25°W, 63.15°N) / -2.8℃, 388mm / Discontinuous Permafrost  
US-BZS:	Bonanza Creek Black Spruce / Fairbanks, Alaska (148.32°W, 64.70°N) / -2.4℃, 274mm / Discontinuous Permafrost  
US-Uaf:	University of Alaska, Fairbanks	/ Fairbanks, Alaska (147.86°W, 64.87°N)	/ -2.9℃, 263mm / Discontinuous Permafrost   
US-Prr:	Poker Flat Research Range Black Spruce Forest / Fairbanks, Alaska (147.49°W, 65.12°N) / -2.0℃, 275mm / Discontinuous Permafrost  
CA-HPC:	Havikpak Creek / Northwest Territories, Canada (133.5°W, 68.33°N) / -8.2℃, 241mm / Continuous Permafrost  


# Eddy covariance and meteorological measurements

Data structure  
    Half-hourly (HH) and daily (DD) original and gap-filled data: 
    *_F means final gap filled products  
    *_ERA and *_W5E5 are the reanalysis downscaled data  
    *_QC are the quality flags:0-measured, 1-filled with high confidence, 2-filled with medium confidence, or 3-filled with low confidence  
    missing data are labeled by -9999   
  
Meteorological data (HH/DD)  
  TA, deg C  
  SW_IN, W m-2  
  LW_IN, W m-2  
  P, mm  
  VPD, hPa  
  PA, kPa  
  WS, m s-1  
  Q, kg/kg  
  RH, %
  
Eddy covariance data (HH/DD)    
  LE, W m-2  
  H, W m-2  
  NEE, umolCO2 m-2 s-1 (HH), gC m-2 d-1 (DD)  
  GPP/GPP_NT/GPP_DT, umolCO2 m-2 s-1 (HH), gC m-2 d-1 (DD)  
  RECO/RECO_NT/RECO_DT, umolCO2 m-2 s-1 (HH), gC m-2 d-1 (DD)  
  
Fluxes daily screening (DD)   
  NEE_crit, NEE daily screening  
  H_crit, H daily screening  
  LE_crit, LE daily screening  
  GPP_GS/GPP_NT_GS/GPP_DT_GS, growing seasons derived by Gonsamo et al. (2013)  
  flags:  
    flag=1, TRUE/YES  
    flag=0, FALSE/NO  
    flag=-9999, Missing/NA  
  
# Ancillary Measurements 

Soil:    
Near-surface soil organic layer depth, cm  
OrgC, g/100g or %  
OrgM, g/100g or %  
Soil texture, USDA texture  
Sand/Silt/Clay, g/100g or %  
Soil permarable depth, meter  
Active layer thicknesses, cm  
Soil driange class, class  
  *(Soil Survey Geographic database, SSURGO) excessively drained, somewhat excessively drained, well drained, moderately well drained, somewhat poorly drained, poorly drained, very poorly drained; (SLC) very rapidly drained, rapidly drained, well drained, moderately well drained, imperfectly drained, poorly drained, and very poorly drained.*  

Vegetation:  
Stand height, meter  
Stand age, years  
Plant functional type (PFT) cover, %   
Species cover, %  
PFT in overstory and understory: evergreen needleleaf forest (ENF), deciduous needleleaf forest (DNF), evergreen broadleaf shrub (EBS), deciduous broadleaf shrub (DBS), C3 grass (C3G), sedge (SDG), and forb (FORB).  
PFT in ground cover: Sphagnum, brown moss, feathermoss, and lichen  

# Canadian Land Surface Scheme including Biogeochemical Cycles (CLASSIC)

CLASSIC is a process-based ecosystem and land surface model designed for use at scales ranging from site-level to global. Environment and Climate Change Canada develops and maintains CLASSIC as an open-source community model.  
More info:  
https://gitlab.com/cccma/classic  
https://cccma.gitlab.io/classic_pages/  
https://cccma.gitlab.io/classic/index.html  

# Authors

Bo Qu, Université de Montréal (Bo.Qu@umontreal.ca; geo_qb@163.com);  
Oliver Sonnentag, Université de Montréal;  
Alexandre Roy, Université du Québec à Trois-Rivières;  
Joe R. Melton, Environment and Climate Change Canada;  
T. Andrew Black, University of British Columbia;  
Brian Amiro, University of Manitoba;      
Eugénie S. Euskirchen, University of Alaska Fairbanks;   
Masahito Ueyama, Osaka Metropolitan University;  
Hideki Kobayashi, Japan Agency for Marine-Earth Science and Technology; 
Christopher Schulze, University of Alberta;  
Gabriel Hould Gosselin, Université de Montréal;  
Alex J. Cannon, Environment and Climate Change Canada;  

# Useful links

AmeriFlux  
CA-Obs, https://ameriflux.lbl.gov/sites/siteinfo/CA-Obs;   
CA-Qfo, https://ameriflux.lbl.gov/sites/siteinfo/CA-Qfo;   
CA-Man, https://ameriflux.lbl.gov/sites/siteinfo/CA-Man;   
CA-SMC, https://ameriflux.lbl.gov/sites/siteinfo/CA-SMC;  
US-BZS, https://ameriflux.lbl.gov/sites/siteinfo/US-BZS;   
US-Prr, https://ameriflux.lbl.gov/sites/siteinfo/US-Prr;   
US-Uaf, https://ameriflux.lbl.gov/sites/siteinfo/US-Uaf;   
CA-HPC, https://ameriflux.lbl.gov/sites/siteinfo/CA-HPC;   

FLUXNET  
https://fluxnet.org/sites/site-list-and-pages/;  

# 

